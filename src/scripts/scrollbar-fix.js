export default function scrollbarFix() {
  const scrollElements = document.getElementsByClassName('u-scrolling-content-scroll-auto');
  if (scrollElements.length > 0) {
    for (let i = 0; i < scrollElements.length; i++) {
      scrollElements[i].setAttribute("data-simplebar", "");
    }
  }
};
