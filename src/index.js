import tippy from 'tippy.js/dist/tippy-bundle.umd.js';
import simplebar from 'simplebar';
import scrollbarFix from "./scripts/scrollbar-fix";
import 'simplebar/dist/simplebar.css';
import 'tippy.js/dist/tippy.css';

export default function chattigojs() {
  scrollbarFix();
};

chattigojs();
