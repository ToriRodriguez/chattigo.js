# Chattigo.js

Dado el crecimiento de la empresa, nuestro [Webkit](https://cdn.chattigo.com/docs/index.html) ha ido añadiendo cada vez más componentes y si bien en algún momento el uso de CSS era más que suficiente, nos encontramos ahora en la necesidad de añadir funcionalidades con Javascript. Para no entorpecer el flujo que se ha ido llevando a cabo, hemos optado por crear una solución sencilla, liviana y accesible: una librería, accesible a través de un CDN.
